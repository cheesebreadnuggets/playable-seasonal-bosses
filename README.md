# "Reign of Runts"

>"Reign of Runts" is a 4 character mod that takes the classic RoG bosses: Deerclops, Bearger, Moose/Goose, and Dragonfly and turns them into Wilson-sized playable characters ripe with perks and personalities! 
Available only for Don't Starve Together on Steam with many content updates since release.

Working title was _'Playable Seasonal Bosses'_

[Art Resource Pack](https://drive.google.com/drive/u/1/folders/1ZCP15ModrqZWmDEjxEpA0lcpbXwnSeHp)

## Shout-outs
- Jack for helping lay some coding groundwork early on in 2019
- Hornet for writing the technical fx code for "Moonbound" Wearger's Lunar Flames in 2024
- Friends in the Robot Pizza Party Discord for participating in an initial playtest before release in 2020
- Dero, Cameo, and Buster for helping film the release trailer in 2020
- Various Content Creators for making videos, streams, fanart, and tweets about Runts over the years and on release
- You :)

## Install a Local Copy
Download the current copy by selecting the Code button (the blue button after 'History' & 'Find file') and select zip under Download source code to download from here!
Add the folder in the zip to your DST mods folder (C:\Program Files (x86)\Steam\SteamApps\common\Don't Starve Together\mods)

![image](https://i.imgur.com/jrZ5sn3.png)
